import logging
from time import sleep

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('pikel')

def main():
    log.info('starting')
    _quit = False
    while not _quit:
        try:
            sleep(1)
        except:
            log.info('ending')
            _quit = True
