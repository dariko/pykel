FROM python:3.7.4-slim

RUN mkdir /opt/pykel
WORKDIR /opt/pykel

ADD requirements.txt /opt/pykel/requirements.txt
RUN pip install -r requirements.txt

ADD . /opt/pykel
RUN pip install .

CMD ["/usr/local/bin/pykel"]
