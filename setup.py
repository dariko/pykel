from setuptools import setup, find_packages

setup(
    name='myapp',
    version='0.0.1',
    author='dariko',
    author_email='git@dariozanzico.com',
    packages=[
        'pykel'
    ],

    python_requires='==3.7.4',

    install_requires=[
        'elasticsearch==7.0.4',
        'kafka-python==1.4.6'
    ],

    entry_points={
        'console_scripts': [
            'pykel=pykel:main',
        ],
    },
)
